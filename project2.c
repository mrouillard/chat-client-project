//worked with Blake Dayman, Jason Campbell, and Sam Amundson
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#define PORT 49153

int main(int argc, char const *argv[])
{
	struct sockaddr_in address;
	int sock = 0, valread;
	struct sockaddr_in serv_addr;
	char message[100] = "";
	char buffer[1024] = {0};
  char username[80];

	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) //creates client socket
	{
		printf("\n Socket creation error \n");
		return -1;
	}

	memset(&serv_addr, '0', sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);
	//if statements from https://www.geeksforgeeks.org/socket-programming-cc/
  if(inet_pton(AF_INET, "10.115.20.250", &serv_addr.sin_addr)<=0)  //looks for server
   {
       printf("\nInvalid address/ Address not supported \n");
       return -1;
   }

   if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)  //connects to server
   {
       printf("\nConnection Failed \n");
       return -1;
   }
   valread = read( sock , buffer, 1024);
   printf("%s\n",buffer );

//username implementation
      printf("Username: \n");
      gets(username);
      send(sock,username,strlen(username),0);
      printf("%s is connecting to the server\n",username);
      bzero(&message, sizeof(message));

//allows user to send multiple messages to server
    while(1) {
      fgets(message, 100, stdin);
      send(sock, message, strlen(message),0);
      bzero(&message, sizeof(message));
//reads messages to server
   valread = read( sock , buffer, 1024);
   printf("%s\n",buffer );
   bzero(&buffer, sizeof(buffer));
 }
   return 0;
}
